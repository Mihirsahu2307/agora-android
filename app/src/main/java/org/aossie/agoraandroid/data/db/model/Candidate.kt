package org.aossie.agoraandroid.data.db.model

data class Candidate(
  var name: String? = null,
  var id: String? = null,
  var party: String? = null
)